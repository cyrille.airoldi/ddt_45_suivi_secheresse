# Projet d'automatisation du suivi des arrêtés sécheresse dans le Loiret

Ce projet regroupe les fichiers sources de l'automatisation et la documentation (en cours de rédaction) facilitant sa portabilité sur d'autres départements utilisant l'infrastructure géoide. 

Les outils utilisés sont :

- R : Création des couches géomatiques à partir du tableau de suivi
- Qgis : Production de la cartographie annéxée lors de la rrédaction des arrêtés
- Géoide carto 2 : cartographie interactive disponible sur internet.

L'ensemble du processus sera testé en situation réelle lors de la campagne 2023.
