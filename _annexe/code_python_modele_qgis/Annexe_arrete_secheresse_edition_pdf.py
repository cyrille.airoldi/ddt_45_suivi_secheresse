"""
Model exported as python.
Name : Annexe arrêté sécheresse - Edition mise en page
Group : SEEF
With QGIS : 31614
"""

from qgis.core import QgsProcessing
from qgis.core import QgsProcessingAlgorithm
from qgis.core import QgsProcessingMultiStepFeedback
from qgis.core import QgsProcessingParameterLayout
from qgis.core import QgsProcessingParameterBoolean
from qgis.core import QgsExpression
import processing


class AnnexeArrtScheresseEditionMiseEnPage(QgsProcessingAlgorithm):

    def initAlgorithm(self, config=None):
        self.addParameter(QgsProcessingParameterLayout('niveaucommunes', 'Niveau communes', defaultValue=None))
        self.addParameter(QgsProcessingParameterLayout('niveauza', 'Niveau ZA', defaultValue=None))
        self.addParameter(QgsProcessingParameterBoolean('VERBOSE_LOG', 'Verbose logging', optional=True, defaultValue=False))

    def processAlgorithm(self, parameters, context, model_feedback):
        # Use a multi-step feedback, so that individual child algorithm progress reports are adjusted for the
        # overall progress through the model
        feedback = QgsProcessingMultiStepFeedback(2, model_feedback)
        results = {}
        outputs = {}

        # Exporter la mise en page au format PDF
        alg_params = {
            'DISABLE_TILED': False,
            'DPI': None,
            'FORCE_VECTOR': False,
            'GEOREFERENCE': False,
            'INCLUDE_METADATA': False,
            'LAYERS': None,
            'LAYOUT': parameters['niveaucommunes'],
            'OUTPUT': QgsExpression('\'M:/_EAU/SECHERESSE/3-Résultats/2023/Annexes carto à l\'\'arrete/\'|| format_date( now(),\'yyyyMMdd\')||\'_\'||\'Niveau communes.pdf\'').evaluate(),
            'SEPARATE_LAYERS': False,
            'SIMPLIFY': True,
            'TEXT_FORMAT': 0,
            'OUTPUT': QgsProcessing.TEMPORARY_OUTPUT
        }
        outputs['ExporterLaMiseEnPageAuFormatPdf'] = processing.run('native:printlayouttopdf', alg_params, context=context, feedback=feedback, is_child_algorithm=True)

        feedback.setCurrentStep(1)
        if feedback.isCanceled():
            return {}

        # Exporter la mise en page au format PDF
        alg_params = {
            'DISABLE_TILED': False,
            'DPI': None,
            'FORCE_VECTOR': False,
            'GEOREFERENCE': False,
            'INCLUDE_METADATA': False,
            'LAYERS': None,
            'LAYOUT': parameters['niveauza'],
            'OUTPUT': QgsExpression('\'M:/_EAU/SECHERESSE/3-Résultats/2023/Annexes carto à l\'\'arrete/\'|| format_date( now(),\'yyyyMMdd\')||\'_\'||\'Niveau ZA.pdf\'').evaluate(),
            'SEPARATE_LAYERS': False,
            'SIMPLIFY': True,
            'TEXT_FORMAT': 0,
            'OUTPUT': QgsProcessing.TEMPORARY_OUTPUT
        }
        outputs['ExporterLaMiseEnPageAuFormatPdf'] = processing.run('native:printlayouttopdf', alg_params, context=context, feedback=feedback, is_child_algorithm=True)
        return results

    def name(self):
        return 'Annexe arrêté sécheresse - Edition mise en page'

    def displayName(self):
        return 'Annexe arrêté sécheresse - Edition mise en page'

    def group(self):
        return 'SEEF'

    def groupId(self):
        return 'SEEF'

    def createInstance(self):
        return AnnexeArrtScheresseEditionMiseEnPage()
